import React from 'react';
import CardBoard from './cardBoard.jsx'
import InfoZone from './infoZone.jsx'
import Controls from './controls.jsx'


import {UNKNOWN_SRC, cardData } from "../data/cardData.js"
import { shuffle } from '../scripts/utils.js';
import "../assets/style/memory.css";


export default class Memory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {cardsData : cardData, UNKNOWN_SRC : UNKNOWN_SRC , playingCards : [], pickedcard : ["pick a card",null], nbPaire : 2,canClick : false,nbOfTry : 0,nbSucess:0, nbPaireNext:2, reset:null};
    this.setPickedCard = this.setPickedCard.bind(this);
    this.changeCanClick = this.changeCanClick.bind(this);
    this.newGame = this.newGame.bind(this);
    this.updateNbPaires = this.updateNbPaires.bind(this);
    this.stopGame = this.stopGame.bind(this);



  }

  



  async componentDidMount(){
      this.setState({ 
        playingCards : shuffle(shuffle(this.state.cardsData).filter((e,i) => i<this.state.nbPaireNext).reduce((elmt,i) => elmt.concat(i,i),[]))});
  }

  newGame(){
    this.setState({
      nbOfTry : 0,
      nbSucess: 0,
      canClick : true,
      nbPaire : this.state.nbPaireNext,
      playingCards : shuffle(shuffle(this.state.cardsData).filter((e,i) => i<this.state.nbPaireNext).reduce((elmt,i) => elmt.concat(i,i),[])),
      pickedcard : ["pick a card",this.state.pickedcard[1]]
      });
  }

  stopGame(){
    if(this.state.reset != null){
      window.clearTimeout(this.state.reset);
      this.setState({reset : null});
    }
    this.setState({canClick : false});
  }


  updateNbPaires(event){
    this.setState({nbPaireNext : parseInt(event.target.value)})
  }

  setPickedCard(card,timeID){

    if(card == null){
      this.setState({pickedcard : ["pick a card",null]})
    }
    else if(this.state.pickedcard[0] == "pick a card"){

      this.setState({pickedcard : [card.id,timeID], nbOfTry : this.state.nbOfTry+1});
    }
    else{
      this.setState({canClick:false});

      if(this.state.pickedcard[0].split("_")[0] == card.id.split("_")[0]){

        window.clearTimeout(this.state.pickedcard[1]);
        window.clearTimeout(timeID);
        this.setState({nbSucess: this.state.nbSucess+1, reset : window.setTimeout(this.changeCanClick,10) });

      }
      else{
        this.setState({reset : window.setTimeout(this.changeCanClick,1500) });
      }
      this.setState({pickedcard :[card.id,timeID] })

    }

  }

  changeCanClick(){
    if(this.state.nbSucess >= this.state.nbPaire){
      this.setState({pickedcard : ["Gagné",null] })
    }
    else{
      this.setState({pickedcard : ["pick a card",null], canClick : true});
    }
  }



  render() {
    const cardboardDiv = <CardBoard canClick ={this.state.canClick} setPickedCard={this.setPickedCard} nbPaire = {this.state.nbPaire} cards={this.state.playingCards} UNKNOWN_SRC={this.state.UNKNOWN_SRC} pickedcard={this.state.pickedcard}/>;
    const infoZoneDiv = <InfoZone remainingPaire={this.state.nbPaire - this.state.nbSucess} last={this.state.pickedcard[0].split("_")[0]} flips={this.state.nbOfTry}/>;
    const controlDiv = <Controls stopGame={this.stopGame} remainingPaire={this.state.nbPaire - this.state.nbSucess} nbPaire={this.state.nbPaire} newGame ={this.newGame} updateNbPaires={this.updateNbPaires} />;
    return (
      <div className="insertReactHere">
        {controlDiv}
        {cardboardDiv}
        {infoZoneDiv}
      </div> 

      );
  }
}
