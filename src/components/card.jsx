import React from 'react';
import "../assets/style/card.css"


export default class card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {src : this.props.UNKNOWN_SRC, visible : false};
    this.swapImage = this.swapImage.bind(this);
    this.backToUknown = this.backToUknown.bind(this);

  }

  swapImage(event){

    if(this.props.canClick){
      if(!this.state.visible){
        const timeID  = setTimeout(this.backToUknown,1500);

        this.props.setPickedCard(event.target,timeID);
        if(!this.state.visible){
          this.setState({src: this.props.src});
        }
        this.setState({visible : !this.state.visible});
        
      }
      
    }

  }


  backToUknown(){
    this.props.setPickedCard(null,);
    this.setState({src:this.props.UNKNOWN_SRC, visible:!this.state.visible});

  }


  render(){
      return <div  className='card'> <img src={this.state.src} alt={this.props.alt} onClick={this.swapImage} id={this.props.id} visible={this.state.visible.toString()} /> </div>;

  }

}