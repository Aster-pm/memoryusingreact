import React from 'react';
import Memory from '../components/memory.jsx';
/*
 define root component
*/
export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>App <Memory/> </div>
    );
  }
}
