import React from 'react';

import Card from "./card.jsx";
import "../assets/style/cardBoard.css"

export default class CardBoard extends React.Component {
  constructor(props) {
    super(props);

  }




  render(){
      const dispCards = this.props.cards.map((elmt,i) => <Card key={elmt.description+"_"+(i)} src={elmt.src} canClick = {this.props.canClick} UNKNOWN_SRC={this.props.UNKNOWN_SRC} alt={elmt.description} id={elmt.description+"_"+(i)} setPickedCard={this.props.setPickedCard}/> );
      return <div> {dispCards} </div>;
  }

}