import React from 'react';
import "../assets/style/controls.css"
import Chrono from "../components/chrono.jsx"

export default class controls extends React.Component {
  constructor(props) {
    super(props);
    this.state = {timer:0,timingID : null, playing : false};
    this.increaseTimer = this.increaseTimer.bind(this);
    this.startTimer = this.startTimer.bind(this);
    this.start = this.start.bind(this);


  }


  startTimer(){
    const intervalID = window.setInterval(this.increaseTimer,1000)
    this.setState({timingID : intervalID });
  }

  stopTimer(){
    window.clearInterval(this.state.timingID);
    this.setState({timingID : null,timer : 0 });
  }

  componentDidUpdate(){
    if(this.props.remainingPaire == 0 && this.state.playing){
      this.stopTimer();

      this.setState({playing:false});
      const play = document.getElementById('play');
      play.innerHTML = "Recommencer";
    }
  }

  start(event){
    if(!this.state.playing || this.props.remainingPaire == 0){

      this.startTimer();
      this.setState({playing:true});
      event.target.innerHTML = "Arrêter";
      this.props.newGame();
    }
    else{
      this.props.stopGame();

      event.target.innerHTML = "Jouer";

      this.stopTimer();
      this.setState({playing:false, timer:0});
    
    }

  }


  increaseTimer(){
    this.setState({timer : this.state.timer+1})
  }



  render(){
    const chronoDiv = <Chrono timer={this.state.timer} />;
      return <div className='controls'> 
      Nb de Paires :<input className='pairs' type="number" min="2" max="15" defaultValue={this.props.nbPaire} onChange = {this.props.updateNbPaires}/> 
      <button id='play' onClick={this.start}>Jouer</button> 
      {chronoDiv} 
      </div>;
  }

}