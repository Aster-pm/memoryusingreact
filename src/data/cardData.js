const IMAGE_ROOT = './images';

const UNKNOWN_SRC = `${IMAGE_ROOT}/inconnu.jpg`;

const cardData = [
  {
    description : 'ane',
    src : `${IMAGE_ROOT}/ane.jpg`,
  },
  {
    description : 'hibou',
    src : `${IMAGE_ROOT}/hibou.jpg`,
  },
  {
    description : 'poule',
    src : `${IMAGE_ROOT}/poule.jpg`,
  },
  {
    description : 'nounours',
    src : `${IMAGE_ROOT}/nounours.jpg`,
  },

  {
    description : 'salameche',
    src : `${IMAGE_ROOT}/salameche.jpg`,
  },

  {
    description : 'riolu',
    src : `${IMAGE_ROOT}/riolu.jpg`,
  },

  {
    description : 'pikachu',
    src : `${IMAGE_ROOT}/pikachu.jpg`,
  },

  {
    description : 'link',
    src : `${IMAGE_ROOT}/link.jpg`,
  },
  {
    description : 'bulbizarre',
    src : `${IMAGE_ROOT}/bulbizarre.jpg`,
  },
  {
    description : 'elchipo',
    src : `${IMAGE_ROOT}/elchipo.jpg`,
  },
  {
    description : 'springtrap',
    src : `${IMAGE_ROOT}/springtrap.jpg`,
  },
  {
    description : 'freddy',
    src : `${IMAGE_ROOT}/freddy.jpg`,
  },
  {
    description : 'donkeykong',
    src : `${IMAGE_ROOT}/donkeykong.jpg`,
  },
  {
    description : 'mario',
    src : `${IMAGE_ROOT}/mario.png`,
  },
  {
    description : 'furby',
    src : `${IMAGE_ROOT}/furby.jpg`,
  },



];

export { UNKNOWN_SRC, cardData } ;
