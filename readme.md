# Projet React Memory 

## Mise en place du Projet

  1. Dans le dossier exécutez
```bash  	  
memoryusingreact$  npm install
```  
  2. Exécutez la commande `npm run build` pour créer le dossier `./dist/` et construire un premier *bundle*
  3. Vous pouvez ouvrir le fichier `dist/index.html`, pour vérifier que tout s'est bien déroulé en consultant la console (<kbd>Ctrl Shift K</kbd>) dans laquelle vous devez lire le message `le bundle a été généré`.  


```bash
memoryusingreact$  npm run dev-server
```
## Description

Un mini jeu Memory développé en HTML/CSS Javascript en utilisant le framework React.

Le projet est composé d'un composant memory.jsx, avec 3 composant enfants principaux (control.jsx, cardBoard.jsx, infoZone.jsx)

- control.jsx : Gère le nombre de paire, le lancement du jeu et le chronomètre (chrono.jsx)
  - chrono.jsx : gère le chronomètre du jeu, se reinitialise à chaque nouvelle partie
- cardBoard.jsx : Gère la dispositions de toutes le cartes (card.jsx)
   - card.jsx : s'occupe d'afficher une carte et de gérer les click et retournement
- infoZone.jsx : affiche le nombre de paires restantes, le nom de la carte dernièrement retournée

### Règles et jeu

Pour commencer à jouer il faut d'abord choisir le nombres de paires en jeu (à gauche du bouton Jouer), et appuyer sur le bouton. Pour retourner une carte il faut cliquer dessus, si vous ne cliquez pas sur une carte en moins de 1.5s la carte se retourne. Lorsque vous cliquez sur deux cartes différentes, vous devez attendre qu'elle se retourne toutes les deux avant de pouvoir recliquer. vous pouvez voir le nombre de paires restantes et le nombre d'essais total. Le jeu se finit lorsque toutes les paires sont retournées Pour lancer une nouvelle partie il faut cliquer sur arrêter et jouer/recommencer.

